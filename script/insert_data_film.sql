-- lancer mysql avec la commande suivante
-- mysql --local-infile=1 -u root -p
SET GLOBAL innodb_buffer_pool_size=2147483648;
-- Vidage des tables
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE Critique;
TRUNCATE TABLE Personnes;
TRUNCATE TABLE Personnalites;
TRUNCATE TABLE Film;
SET FOREIGN_KEY_CHECKS = 1;

-- Insertion des données à partir du fichier TSV
LOAD DATA INFILE '/var/lib/mysql-files/film/title.basics/data.tsv'
INTO TABLE Film
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(id, @dummy, titre, @dummy, @dummy, @date, @dummy, @duree, @genres)
SET date = NULLIF(@date, '-1'),
    duree = NULLIF(@duree, '-1'),
    genres = NULLIF(@genres, '-1');

LOAD DATA INFILE '/var/lib/mysql-files/film/name.basics/data.tsv'
INTO TABLE Personnalites
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(id, nom, @dummy, @dummy, @profession, @dummy)
SET profession = NULLIF(@profession, '-1');

LOAD DATA INFILE '/var/lib/mysql-files/film/title.principals/data.tsv' IGNORE
INTO TABLE Personnes
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
(id_film, @dummy, id_personnalites, @poste, @dummy, @personnage)
SET poste = NULLIF(@poste, '-1'),
    personnage = NULLIF(@personnage, '-1');

LOAD DATA INFILE '/var/lib/mysql-files/film/title.ratings/data.tsv' IGNORE
INTO TABLE Critique
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
(id_film, notation, nombreVotes);