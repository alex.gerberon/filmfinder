###########################
#     Trie title.basics   #
###########################
# Sp�cifiez le chemin d'acc�s au fichier CSV d'entr�e
$chemin = ".\title.basics.tsv\data.tsv"

# Sp�cifiez le chemin d'acc�s au fichier CSV de sortie
$chemin_sortie = ".\title.basics.tsv\data_trie.tsv"

# Ouvrir le fichier d'entr�e en lecture
$lecteur = [System.IO.File]::OpenText($chemin)

# Cr�er un fichier de sortie
$fichier_sortie = [System.IO.File]::CreateText($chemin_sortie)

# Lire la premi�re ligne du fichier d'entr�e (en-t�te) et l'�crire dans le fichier de sortie
$enTete = $lecteur.ReadLine()
$fichier_sortie.WriteLine($enTete)

# Parcourir le reste du fichier d'entr�e
while ($ligne = $lecteur.ReadLine()) {
    # Cr�er un objet contenant les valeurs de la ligne actuelle
    $objet = $ligne -split "`t"
    # V�rifier si la ligne correspond � un film
    if ($objet[1] -eq "movie") {
        $objet = $objet | ForEach-Object {
            $_ -replace '\\N', '-1'
            }
        $ligne = $objet -join "`t"
        # �crire la ligne tri�e dans le fichier de sortie
        $fichier_sortie.WriteLine($ligne)
    }
}
# Fermer les fichiers d'entr�e et de sortie
$lecteur.Close()
$fichier_sortie.Close()

###########################
#  Trie title.principals  #
###########################
# Sp�cifiez le chemin d'acc�s au fichier CSV d'entr�e
$chemin = ".\title.principals.tsv\data.tsv"

# Sp�cifiez le chemin d'acc�s au fichier CSV de sortie
$chemin_sortie = ".\title.principals.tsv\data_trie.tsv"

# Ouvrir le fichier d'entr�e en lecture
$lecteur = [System.IO.File]::OpenText($chemin)

# Cr�er un fichier de sortie
$fichier_sortie = [System.IO.File]::CreateText($chemin_sortie)

# Lire la premi�re ligne du fichier d'entr�e (en-t�te) et l'�crire dans le fichier de sortie
$enTete = $lecteur.ReadLine()
$fichier_sortie.WriteLine($enTete)

# Parcourir le reste du fichier d'entr�e
while ($ligne = $lecteur.ReadLine()) {
    # Cr�er un objet contenant les valeurs de la ligne actuelle
    $objet = $ligne -split "`t"

    # V�rifier si la ligne correspond � une cat�gorie pertinente
    if ($objet[3] -match "director|composer|producer|actor|actress") {
        $objet = $objet | ForEach-Object {
            $_ -replace '\\N', '-1'
            }
        $ligne = $objet -join "`t"
        # �crire la ligne tri�e dans le fichier de sortie
        $fichier_sortie.WriteLine($ligne)
    }
}

# Fermer les fichiers d'entr�e et de sortie
$lecteur.Close()
$fichier_sortie.Close()

###########################
#     Trie name.basics    #
###########################
# Sp�cifiez le chemin d'acc�s au fichier CSV d'entr�e
$chemin = ".\name.basics.tsv\data.tsv"

# Sp�cifiez le chemin d'acc�s au fichier CSV de sortie
$chemin_sortie = ".\name.basics.tsv\data_trie.tsv"

# Ouvrir le fichier d'entr�e en lecture
$lecteur = [System.IO.File]::OpenText($chemin)

# Cr�er un fichier de sortie
$fichier_sortie = [System.IO.File]::CreateText($chemin_sortie)

# Lire la premi�re ligne du fichier d'entr�e (en-t�te) et l'�crire dans le fichier de sortie
$enTete = $lecteur.ReadLine()
$fichier_sortie.WriteLine($enTete)

# Parcourir le reste du fichier d'entr�e
while ($ligne = $lecteur.ReadLine()) {
    # Cr�er un objet contenant les valeurs de la ligne actuelle
    $objet = $ligne -split "`t"

    # V�rifier si la ligne correspond � une profession pertinente
    if ($objet[4] -match "director|composer|producer|actor|actress") {
        $objet = $objet | ForEach-Object {
            $_ -replace '\\N', '-1'
            }
        $ligne = $objet -join "`t"
        # �crire la ligne tri�e dans le fichier de sortie
        $fichier_sortie.WriteLine($ligne)
    }
}

# Fermer les fichiers d'entr�e et de sortie
$lecteur.Close()
$fichier_sortie.Close()