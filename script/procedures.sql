DROP PROCEDURE IF EXISTS GetDramaFilms;
DROP PROCEDURE IF EXISTS GetFilmsByYear;
DROP PROCEDURE IF EXISTS GetCurrentYearComedyFilms;
DROP PROCEDURE IF EXISTS GetFilmsByYearRange;
DROP PROCEDURE IF EXISTS GetFilmsByDirector;


DELIMITER //
CREATE PROCEDURE GetDramaFilms()
BEGIN
    SELECT * FROM Film WHERE genres LIKE '%Drama%';
END //




CREATE PROCEDURE GetFilmsByYear(IN year INT)
BEGIN
    SELECT * FROM Film WHERE date = year;
END //




CREATE PROCEDURE GetCurrentYearComedyFilms()
BEGIN
    SELECT * FROM Film WHERE genres LIKE '%Comedy%' AND date = YEAR(CURDATE());
END //




CREATE PROCEDURE GetFilmsByYearRange(IN start_year INT, IN end_year INT)
BEGIN
    SELECT * FROM Film WHERE date BETWEEN start_year AND end_year;
END //




CREATE PROCEDURE GetFilmsByDirector(IN director_name VARCHAR(255))
BEGIN
    SELECT F.* 
    FROM Film AS F
    INNER JOIN Personnes AS Pe ON F.id = Pe.id_film
    INNER JOIN Personnalites AS P ON Pe.id_personnalites = P.id
    WHERE P.nom = director_name AND Pe.poste = 'director';
END //
DELIMITER ;




-- -- Pour tester la procédure GetDramaFilms
-- CALL GetDramaFilms();

-- -- Pour tester la procédure GetFilmsByYear (par exemple, pour récupérer les films de l'année 2020)
-- CALL GetFilmsByYear(2020);


-- -- Pour tester la procédure GetCurrentYearComedyFilms
-- CALL GetCurrentYearComedyFilms();


-- CALL GetFilmsByYearRange(2000, 2010);

-- CALL GetFilmsByDirector('Christopher Nolan');


