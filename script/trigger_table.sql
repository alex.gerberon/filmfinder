DELIMITER //

CREATE TRIGGER tr_check_duration_before_insert
BEFORE INSERT ON Film
FOR EACH ROW
BEGIN
    IF NEW.duree < 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La durée du film doit être supérieure à zéro.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_duration_before_update
BEFORE UPDATE ON Film
FOR EACH ROW
BEGIN
    IF NEW.duree < 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La durée du film doit être supérieure à zéro.';
    END IF;
END; //

DELIMITER ;



DELIMITER //

CREATE TRIGGER tr_check_notation_before_insert
BEFORE INSERT ON Critique
FOR EACH ROW
BEGIN
    IF NEW.notation < 0 OR NEW.notation > 10 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La notation doit être entre 0 et 10.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_notation_before_update
BEFORE UPDATE ON Critique
FOR EACH ROW
BEGIN
    IF NEW.notation < 0 OR NEW.notation > 10 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La notation doit être entre 0 et 10.';
    END IF;
END; //

DELIMITER ;


DELIMITER //

CREATE TRIGGER tr_check_nombreVotes_before_insert
BEFORE INSERT ON Critique
FOR EACH ROW
BEGIN
    IF NEW.nombreVotes < 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le nombre de votes doit être supérieur ou égal à zéro.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_nombreVotes_before_update
BEFORE UPDATE ON Critique
FOR EACH ROW
BEGIN
    IF NEW.nombreVotes < 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le nombre de votes doit être supérieur ou égal à zéro.';
    END IF;
END; //

DELIMITER ;


DELIMITER //

CREATE TRIGGER tr_check_nom_before_insert
BEFORE INSERT ON Personnalites
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.nom) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le nom ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_nom_before_update
BEFORE UPDATE ON Personnalites
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.nom) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le nom ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;


DELIMITER //

CREATE TRIGGER tr_check_titre_before_insert
BEFORE INSERT ON Film
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.titre) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le titre du film ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_titre_before_update
BEFORE UPDATE ON Film
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.titre) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le titre du film ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;


DELIMITER //

CREATE TRIGGER tr_check_poste_before_insert
BEFORE INSERT ON Personnes
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.poste) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le poste ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_poste_before_update
BEFORE UPDATE ON Personnes
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.poste) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le poste ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_profession_before_insert
BEFORE INSERT ON Personnalites
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.profession) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La profession ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_profession_before_update
BEFORE UPDATE ON Personnalites
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.profession) = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La profession ne peut pas être vide.';
    END IF;
END; //

DELIMITER ;


DELIMITER //

CREATE TRIGGER tr_check_date_before_insert
BEFORE INSERT ON Film
FOR EACH ROW
BEGIN
    IF NEW.date > YEAR(CURDATE()) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La date du film ne peut pas être dans le futur.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_date_before_update
BEFORE UPDATE ON Film
FOR EACH ROW
BEGIN
    IF NEW.date > YEAR(CURDATE()) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La date du film ne peut pas être dans le futur.';
    END IF;
END; //

DELIMITER ;


DELIMITER //

CREATE TRIGGER tr_check_personnage_length_before_insert
BEFORE INSERT ON Personnes
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.personnage) > 500 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le nom du personnage ne peut pas dépasser 500 caractères.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_personnage_length_before_update
BEFORE UPDATE ON Personnes
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.personnage) > 500 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le nom du personnage ne peut pas dépasser 500 caractères.';
    END IF;
END; //

DELIMITER ;


DELIMITER //

CREATE TRIGGER tr_check_genres_length_before_insert
BEFORE INSERT ON Film
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.genres) > 255 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le genre du film ne peut pas dépasser 255 caractères.';
    END IF;
END; //

DELIMITER ;

DELIMITER //

CREATE TRIGGER tr_check_genres_length_before_update
BEFORE UPDATE ON Film
FOR EACH ROW
BEGIN
    IF LENGTH(NEW.genres) > 255 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le genre du film ne peut pas dépasser 255 caractères.';
    END IF;
END; //

DELIMITER ;
