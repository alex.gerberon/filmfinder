-- Un utilisateur recherche tous les films d'action.
SELECT title FROM Film WHERE genres LIKE '%Action%';

-- Un utilisateur souhaite voir les films d'action les mieux notés sur plus de 1000 votes.
SELECT titre FROM Film
JOIN Critique ON Film.id = Critique.id_film
WHERE genres LIKE '%Action%' AND nombreVotes > 1000
ORDER BY Critique.notation DESC
LIMIT 5;

-- Un utilisateur veut voir les critiques du film Taxi 5.
SELECT notation, nombreVotes FROM Critique WHERE titre EQUALS 'taxi 5';

-- Un utilisateur veut trouver tous les films d'aventure sortis entre 2010 et 2020 qui ont une note moyenne supérieure à 7.
SELECT titre, date FROM Film
JOIN Critique ON Film.id = Critique.id_film
WHERE genres LIKE '%Adventure%' AND notation > 7
AND date BETWEEN 2010 AND 2020;

-- Un utilisateur veut comparer Taxi 5 et Taxi 4 sur la base des critiques.
SELECT Film1.titre AS film1, Film2.titre AS film2, Critique1.notation AS note1, Critique2.notation AS note2
FROM Film AS Film1
JOIN Film AS Film2 ON Film1.titre = 'Taxi 5' AND Film2.titre = 'Taxi 4'
JOIN Critique AS Critique1 ON Film1.id = Critique1.id_film
JOIN Critique AS Critique2 ON Film2.id = Critique2.id_film;

-- Un utilisateur veut trouver les films où joue Jean Reno.
SELECT titre FROM Film
JOIN Personnes ON Film.id = Personnes.id_film
JOIN Personnalites ON Personnes.id_personnalites = Personnalites.id
WHERE Personnalites.nom = 'Jean Reno';

-- Un utilisateur veut voir tous les films sortis en 2023 et qui a une durée de plus de 2 heures.
SELECT titre, date, duree FROM Film
WHERE date = 2023 AND duree > 120;

-- Un utilisateur veut voir tous les films ayant une note supérieure à 7/10 sur plus de 1000 avis.
SELECT titre, notation, nombreVotes FROM Film
JOIN Critique ON Film.id = Critique.id_film
WHERE notation > 9 AND nombreVotes > 1000;

-- Un utilisateur veut voir le film le mieux noté produit par Luc Besson.
SELECT titre, notation
FROM Film
JOIN Critique ON Film.id = Critique.id_film
WHERE Film.id IN (
    SELECT id_film
    FROM Personnes
    JOIN Personnalites ON Personnes.id_personnalites = Personnalites.id
    WHERE Personnalites.nom = 'Luc Besson'
)
ORDER BY Critique.notation DESC
LIMIT 1;

-- Un utilisateur veut voir le genre de film le plus populaire.
SELECT genres, SUM(nombreVotes) AS totalVotes FROM Film
JOIN Critique ON Film.id = Critique.id_film
GROUP BY genres
ORDER BY totalVotes DESC
LIMIT 1;

-- Un utilisateur veut voir les films ayant une note supérieur à la moyenne de toutes les critiques.
SELECT titre, notation FROM Film
JOIN Critique ON Film.id = Critique.id_film
WHERE notation > (SELECT AVG(notation) FROM Critique);

-- Un utilisateur veut savoir la durée moyenne des films d'action sorties en 2023.
SELECT AVG(duree) FROM Film
WHERE genres LIKE '%Action%' AND date = 2023;

-- Un utilisateur veut voir le réalisateur (director) qui a dirigé le plus de films de sciences fiction dans sa carrière.
SELECT Personnalites.nom, COUNT(*) AS totalFilms FROM Film
JOIN Personnes ON Film.id = Personnes.id_film
JOIN Personnalites ON Personnes.id_personnalites = Personnalites.id
WHERE Personnes.poste = 'director' AND genres LIKE '%Sci-Fi%'
GROUP BY Personnalites.nom
ORDER BY totalFilms DESC
LIMIT 1;

-- Un utilisateur veut connaitre le casting du film Avatar.
SELECT Personnalites.nom, Personnes.poste
FROM Personnes
JOIN Personnalites ON Personnes.id_personnalites = Personnalites.id
WHERE Personnes.id_film IN (SELECT id FROM Film WHERE titre = 'Avatar');